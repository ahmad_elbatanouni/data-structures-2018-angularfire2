import { Component, OnInit, ViewChildren, AfterViewChecked, ElementRef, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { AngularFireStorage } from 'angularfire2/storage';

import { Observable } from 'rxjs';
import { map, finalize, filter } from 'rxjs/operators';
import { Router } from "@angular/router";
import { AuthService } from '../auth.service';

declare let Date, Math;

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  id: string;
  message: string;
  user$: any;

  other_party: any;

  messageCollectionRef: AngularFirestoreCollection<any>;
  message$: Observable<any[]>;

  list_of_users = {};
  users_array: any[];
  
  names_array: any[] = [];



  messages: any[] = [];
  constructor(private route: ActivatedRoute,
              private afs: AngularFirestore,
              public auth: AuthService,
              private storage: AngularFireStorage,
              private router: Router) {
    
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

    
    this.auth.user.subscribe(user => {
      this.user$ = user;
      
      // the IF is responsible for getting SPECIFIC messages
      // while the ELSE is for getting the list of users
      if(this.id != null) {
        this.afs.collection<any>('users', ref => ref.where('uid', '==', this.id))
          .valueChanges().subscribe(user => {
            this.other_party = user[0];
          });

        this.messageCollectionRef = this.afs.collection<any>('messages', ref => {
          return ref.where('users', 'array-contains', this.user$.uid);
        });

        this.message$ = this.messageCollectionRef.snapshotChanges().
        pipe(
          map(actions => {
            this.messages = [];
            let x = actions.map(action => { 
              let data = action.payload.doc.data();
              let id = action.payload.doc.id;
              if(data['users'].indexOf(this.id) > -1) {
                // this.messages.push({id, data});
              
                // set the message 'is_read' to TRUE, if not already
                if(data['to'] == this.user$.uid && !data['is_read'])
                  this.update(id);
                
                return {id, data};
              }              
            });
            x.sort( (a, b) => {
              if (a.data.Date < b.data.Date)
                return -1;
              if (a.data.Date > b.data.Date)
                return 1;
              return 0;
            });
            return x;
          })
        );// End of message$ observable
        
      } // End of if statement
      else {
        this.messageCollectionRef = this.afs.collection<any>('messages', ref => {
          return ref.where('users', 'array-contains', this.user$.uid);
        });

        this.message$ = this.messageCollectionRef.snapshotChanges().
        pipe(
          map(actions => {     
            
            this.list_of_users = {};
            this.users_array = [];
            this.names_array = [];       
            let x = actions.map(action => { 
              let data = action.payload.doc.data();
              let users_array = data['users'];
              // console.log(data['is_read']);
              // console.log(this.list_of_users[users_array[1]] + " | " + (users_array[1] == this.user$.uid));

              // console.log(this.list_of_users[users_array[0]] + " | " + (users_array[0] == this.user$.uid));
              // console.log('-----------------------');

              // if(users_array[0] == this.user$.uid) {
              if(data['from'] == this.user$.uid) {
                if(this.list_of_users[data['to']] == undefined) {
                  if(data['is_read'])
                    this.list_of_users[data['to']] = 0;
                  else if (data['to'] == this.user$.uid)
                    this.list_of_users[data['to']] = 1;
                }
                else {                  
                  if(!data['is_read'] && data['to'] == this.user$.uid)
                    this.list_of_users[data['to']]++;
                }
              } else {
                if(this.list_of_users[data['from']] == undefined) {
                  if(data['is_read'])
                    this.list_of_users[data['from']] = 0;
                  else if (data['to'] == this.user$.uid)
                    this.list_of_users[data['from']] = 1;
                }
                else {                  
                  if(!data['is_read'] && data['to'] == this.user$.uid)
                    this.list_of_users[data['from']]++;
                }
              }
              return {id: action.payload.doc.id};
            });

            // users_array holds the users IDs
            this.users_array = Object.keys(this.list_of_users);
            
            this.users_array.forEach((user_id, index) => {              
              this.afs.collection<any>('users', ref => ref.where('uid', '==', user_id))
                .valueChanges().subscribe(user => {                  
                  this.names_array[index] = {
                    // indeed we're looking for SINGLE user,
                    // but firebase returns an ARRAY of ONE Object
                    user: user[0], 
                    new_messages: this.list_of_users[user[0].uid]
                  };


                
                });
            });            
            return x;
          })
        );// End of if message$ observable
        this.message$.subscribe(a => {});
      } // End of else statement

      if(this.id == this.user$.uid) {
        this.router.navigate(['/messages']);
      }
    });

    
    // check if ID exists, if not --> redirect

  }


  @ViewChildren('scrollMe') myScrollContainer: QueryList<any>;
  ngAfterViewInit() {
    this.myScrollContainer.changes.subscribe(t => {
      if(document.getElementById('chat_box'))
        document.getElementById('chat_box').scrollTop = document.getElementById('chat_box').scrollHeight;     
    });   
  } 



  compare(a,b) {
    if (a.data.Date < b.data.Date)
      return -1;
    if (a.data.Date > b.data.Date)
      return 1;
    return 0;
  }

  delete(id) {
    // console.log(id);
    try{
      this.afs.collection<any>('messages').doc(id).delete();
    } catch(err) {
      console.log(err);      
    }
  }

  update(id) {
    this.messageCollectionRef.doc(id).set({ is_read: true }, { merge: true });
  }

  async send() {
    let message = {
      users: [this.user$.uid, this.id],
      from: this.user$.uid,
      to: this.id,
      message: this.message,
      Date: Date.now(),
      is_read: false
    };

    if (this.file_upload_started) {
      if(this.filesrc == null)
        alert('Wait for the upload to complete.');
      if(this.is_image)
        message['type'] = 'image';
      else {
        message['type'] = 'upload';
        message['file_name'] = this.file_name;
      }
      await this.filesrc.subscribe(url => {        
        message['message'] = url;
      });
    }
    
    this.messageCollectionRef.add(message);

    if (!this.file_added)
      this.message = "";
    else {
      this.filesrc = null;
      this.file_upload_started = false;
      this.file_added = null;
      this.progressBarValue = null;
      this.selectedFiles = null;
      this.is_image = false;
      this.file_name = '';
    }
  }
  
  enterClicked(event) {
    if(event.keyCode == 13) this.send();
  }

  selectedFiles: any;
  filesrc: Observable<any>;
  file_name: string; // used with the non-image uploads
  is_image: boolean = false;
  file_added: boolean = false;
  file_upload_started: boolean = false;
  progressBarValue: Observable<any>;

  chooseFiles(event) {
    this.selectedFiles = event.target.files;
    if(this.selectedFiles.item(0)) {
      if(this.selectedFiles.item(0).type.match(/image\/.*/g)) {
        this.is_image = true;
        this.upload();
        return;
      }      
      let types = ['application/pdf', 'application/x-rar-compressed', 'application/zip',
                  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                  'application/msword'];
      if (types.includes(this.selectedFiles.item(0).type)) {
        this.upload();
        return;
      }
      
      alert("only images, PDFs, Word, or compressed(zip, rar) files are allowed.");
    }
  }

  upload() {
    this.file_upload_started = true;
    let file = this.selectedFiles.item(0);
    
    let types = {'application/pdf' : 'pdf',
                 'application/x-rar-compressed': 'rar',
                 'application/zip': 'zip',
                 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx',
                 'application/msword': 'doc',
                 'image/bmp': 'bmp',
                 'image/gif': 'gif',
                 'image/jpeg': 'jpg',
                 'image/png': 'png',
                 'image/webp': 'wepb'
                };
    
    let uniqkey = 'ds' + Math.floor(Math.random() * 1000000) + "." + types[file.type];
    const uploadTask = this.storage.upload('/ds/' + uniqkey, file);
    const ref = this.storage.ref('/ds/' + uniqkey);

    this.progressBarValue = uploadTask.percentageChanges();

    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        this.file_name = file.name;
        this.filesrc = ref.getDownloadURL();
        this.file_added = true;
        this.progressBarValue = null;
      })
    ).subscribe();
  }
  

}
