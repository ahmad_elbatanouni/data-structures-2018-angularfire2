import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { trigger,style,transition,animate,state } from '@angular/animations';
import { map } from 'rxjs/operators';

declare let CodeMirror: any;

@Component({
  selector: 'app-grades',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.css'],
  animations: [
    trigger('slide', [
      state('in',
        style({
          opacity: 1,
          transform: 'translateY(0%)',
          'margin-bottom': '10px',
        })),
      state('out',
        style({
          opacity: 0,
          transform: 'translateY(-100%)',
          'margin-bottom': '10px',
          height: 0,
        })),
      transition('in => out',animate('200ms ease-in-out')),
      transition('out => in',animate('200ms ease-in-out'))
    ]),
  ]
})
export class GradesComponent implements OnInit {

  global_editor: any;
  editor_visiblility: String = 'out';
  grade$: Observable<any[]>;
  grades_sub: any;

  is_new: object = {};

  constructor(private db: AngularFirestore, public auth: AuthService) { }

  ngOnInit() {
    

    this.auth.user.subscribe(user => {
      
      this.grade$ = this.db.collection('students_answers', ref => ref.where('uid', '==', user['uid']))
        .snapshotChanges().pipe(

          map(actions => {            
            return actions.map(action => { 
              let data = action.payload.doc.data();
              let id = action.payload.doc.id;
              if(data['date']) 
                data['date'] = new Date(data['date']).toLocaleDateString() + ", " + new Date(data['date']).toLocaleTimeString();

              return { id, ...data };
            });
          })
        );

        this.grades_sub = this.grade$.subscribe(grades => {
          grades.forEach(grade => { 
            if(grade['status'] != 'uploaded' && grade['notification_read'] == 0) {
              this.db.collection('students_answers', ref => ref.where('uid', '==', user['uid']))
                .doc(grade.id).set({ notification_read: 1 }, { merge: true });
                // console.log(is_new);
                
                this.is_new[grade.id] = true;
            }
          });
          
        })
      
    }); // user observable
  }

  ngAfterViewInit() {    
    this.global_editor = CodeMirror.fromTextArea(document.getElementById('global_editor'), {
      theme: 'oceanic-next',
      lineNumbers: true,
      mode: 'text/x-java',
      matchBrackets: true,
      height: '30px'
    });
  }

  fill(code) {
    this.editor_visiblility = 'in';
    this.global_editor.setValue(code);
    this.global_editor.focus();
  }

  close() {
    this.editor_visiblility = 'out';
  }
  

  ngOnDestroy() {
    this.grades_sub.unsubscribe();
  }

}
