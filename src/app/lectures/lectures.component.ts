import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-lectures',
  templateUrl: './lectures.component.html',
  styleUrls: ['./lectures.component.css']
})
export class LecturesComponent implements OnInit {
  data: any;
  id: any;
  lectures$: Observable<any[]>;
  constructor(db: AngularFirestore) {
    this.lectures$ = db.collection('lectures', ref => ref.orderBy('lec_number', "desc")).valueChanges();
    // .snapshotChanges().pipe(
    //   map(actions => {
    //     return actions.map(action => {
    //       this.data = action.payload.doc.data();
    //       this.id = action.payload.doc.id;
    //       return { id: this.id, data: this.data };
    //     });
    //   })
    // );
  }

  ngOnInit() {
  }

}
