import { Component, ViewChildren, QueryList } from '@angular/core';
import { AuthService } from '../auth.service';

import { AngularFireStorage } from 'angularfire2/storage';

import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AngularFirestore } from 'angularfire2/firestore';


declare let CodeMirror: any;

@Component({
  selector: 'app-assignments',
  templateUrl: './assignments.component.html',
  styleUrls: ['./assignments.component.css']
})
export class AssignmentsComponent  {

  code_samples$: Observable<any[]>;
  code_samplesArray: any[];
  assignments$: Observable<any[]>;

  editors: any [] = [];
  dataBack: string;

  user$: any;
  
  lec_title_filter: String = '';

  constructor(private db: AngularFirestore,
              private storage: AngularFireStorage,
              private route: ActivatedRoute,
              public auth: AuthService) {
    this.assignments$ = db.collection('assignments', ref => ref.orderBy('assignment_number', 'desc')).valueChanges();
  }

  ngOnInit() {

    if(this.route.snapshot.paramMap.get('lecture_title'))
      this.lec_title_filter = this.route.snapshot.paramMap.get('lecture_title');

    this.auth.user.subscribe(user => {
      this.user$ = user;
    });
  }



  @ViewChildren('listOfAssignments') things: QueryList<any>;
  ngAfterViewInit() {
    this.things.changes.subscribe(t => {      
      // Make sure old stuff is cleared
      this.editors.forEach(editor=> {
        editor.toTextArea();
      })


      let tas = document.getElementsByClassName('tas');
      for (let i = 0; i < tas.length; i++) {
        if(document.getElementById(tas[i].id)) {
          let ta = document.getElementById(tas[i].id);
          let index = +tas[i].id.slice(3, tas[i].id.length);

          this.editors[index] = CodeMirror.fromTextArea(ta, {
            theme: 'oceanic-next',
            lineNumbers: true,
            mode: 'text/x-java',
            matchBrackets: true,
            height: '30px'
          });
            
          this.editors[index].setValue("// place your answer here");
        }
      }      
    });
  }
  

  async submit(view_index, assignment) {
    if(this.user$) {
      
      let answer = ( assignment.type == "code" ) ? this.editors[view_index].getValue() : "get uploaded file link";
      if( assignment.type == 'file' && !this.file_added) {
        alert("Upload a file first.")
        return;
      }        
      let data_to_store = {
        type: assignment.type,
        uid: this.user$.uid,
        assignment_number: assignment.assignment_number,
        answer,
        status: "uploaded",
        notification_read: 0,
        date: Date.now(),
      };

      if (this.file_upload_started) {
        if(this.filesrc == null)
          alert('Wait for the upload to complete.');
   
        await this.filesrc.subscribe(url => {        
          data_to_store['answer'] = url;
        });
      }
      // console.log(data_to_store);
      

      this.db.collection('students_answers').add(data_to_store).then(success => {
        alert('Sent successfully.');
        this.filesrc = null;
        this.file_upload_started = false;
        this.file_added = null;
        this.progressBarValue = null;
        this.selectedFiles = null;

      }).catch(e => {
        if (e)
          alert('You don\'t have the right to do this.');      
      });
    }
  }

  
  selectedFiles: any;
  filesrc: Observable<any>;
  file_added: boolean = false;
  file_upload_started: boolean = false;
  progressBarValue: Observable<any>;

  chooseFiles(event) {
    this.selectedFiles = event.target.files;
    if(this.selectedFiles.item(0)) {
      let types = ['application/pdf', 'application/x-rar-compressed', 'application/zip',
                  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                  'application/msword'];
      if (types.includes(this.selectedFiles.item(0).type)) {
        this.upload();
        return;
      }
      
      alert("only PDFs, Word, or compressed(zip, rar) files are allowed.");
    }
  }

  upload() {
    this.file_upload_started = true;
    let file = this.selectedFiles.item(0);
    let types = {'application/pdf' : 'pdf',
                 'application/x-rar-compressed': 'rar',
                 'application/zip': 'zip',
                 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx',
                 'application/msword': 'doc',
                };
    
    let uniqkey = 'ds' + Math.floor(Math.random() * 1000000) + "." + types[file.type];
    const uploadTask = this.storage.upload('/ds/' + uniqkey, file);
    const ref = this.storage.ref('/ds/' + uniqkey);

    this.progressBarValue = uploadTask.percentageChanges();

    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        this.filesrc = ref.getDownloadURL();
        this.file_added = true;
        this.progressBarValue = null;
      })
    ).subscribe();
  }

}
