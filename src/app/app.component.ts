import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public auth: AuthService) {}
  
  ToggleMenu(e) {
    let x = document.getElementById("navbar-burger");
    let y = document.getElementById("navbar-menu");
    if (x.className.indexOf("is-active") == -1) {
      x.className += " is-active";
      y.className += " is-active";
    } else {
      x.className = "navbar-burger";
      y.className = "navbar-menu";
    }
    
    e.stopPropagation();
  }
}
