import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {Routes, RouterModule} from "@angular/router";
import { environment } from '../environments/environment';
import { SafeHTMLPipe } from './safe-html.pipe';

import { routes } from './routes';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { AppComponent } from './app.component';
import { LecturesComponent } from './lectures/lectures.component';
import { CodeSamplesComponent } from './code-samples/code-samples.component';
import { AssignmentsComponent } from './assignments/assignments.component';
import { UserComponent } from './user/user.component';
import { SavedCodeSamplesComponent } from './saved-code-samples/saved-code-samples.component';
import { MessagesComponent } from './messages/messages.component';
import { GradesComponent } from './grades/grades.component';
import { SearchPipe } from './search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SafeHTMLPipe,
    LecturesComponent,
    CodeSamplesComponent,
    AssignmentsComponent,
    UserComponent,
    SavedCodeSamplesComponent,
    MessagesComponent,
    GradesComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    RouterModule.forRoot(routes),
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
