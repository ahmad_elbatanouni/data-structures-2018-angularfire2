import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedCodeSamplesComponent } from './saved-code-samples.component';

describe('SavedCodeSamplesComponent', () => {
  let component: SavedCodeSamplesComponent;
  let fixture: ComponentFixture<SavedCodeSamplesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedCodeSamplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedCodeSamplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
