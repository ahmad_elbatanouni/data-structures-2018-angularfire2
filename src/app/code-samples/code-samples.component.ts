import { Component, ViewChildren, QueryList } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { trigger,style,transition,animate,state } from '@angular/animations';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs';

import { AuthService } from '../auth.service';

declare let CodeMirror: any;

@Component({
  selector: 'app-code-samples',
  templateUrl: './code-samples.component.html',
  styleUrls: ['./code-samples.component.css'],
  animations: [
    trigger('slide', [
      state('in', style({opacity: 1, transform: 'translateY(0%)'})),
      state('out', style({opacity: 0, transform: 'translateY(-200%)'})),
      transition('in => out',animate('200ms ease-in-out')),
      transition('out => in',animate('200ms ease-in-out'))
    ]),
    trigger('outputSlide', [
      state('in',
        style({
          opacity: 1,
          transform: 'translateY(0%)',
          'margin-bottom': '10px',
        })),
      state('out',
        style({
          opacity: 0,
          transform: 'translateY(-100%)',
          'margin-bottom': '10px',
          height: 0,
        })),
      transition('in => out',animate('200ms ease-in-out')),
      transition('out => in',animate('200ms ease-in-out'))
    ]),
  ]
})
export class CodeSamplesComponent  {

  user$: any;
  code_samples$: Observable<any[]>;
  code_samplesArray: any[];
  constructor(private db: AngularFirestore, private http: Http, public auth: AuthService) {
    this.code_samples$ = db.collection('marks').snapshotChanges()
      .pipe(
        map(actions => {
          this.code_samplesArray = [];
          return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          this.outputSlideState[this.outputSlideStateCounter++] = 'out';
          this.code_samplesArray.push({ id, ...data });
          return { id, ...data };
          })
        })
      );
  }

  ngOnInit() {
    this.auth.user.subscribe(user => {
      this.user$ = user;
    });
  }

  editors: any [] = [];
  dataBack: string;

  shown: boolean = true;
  slideState: string = 'out';
  inputSlideState: string = 'out';
  outputSlideState: string [] = [];
  outputSlideStateCounter: number = 0;

  cs_filter:string = '';

  global_code: string = '';
  global_title: string = '';

  @ViewChildren('listOfCodeSamples') things: QueryList<any>;
  ngAfterViewInit() {
    this.things.changes.subscribe(t => {

      // Make sure old stuff is cleared
      for (let index = 0; index < this.editors.length; index++) 
        this.editors[index].toTextArea();

      for (let index = 0; index < this.code_samplesArray.length; index++) {
        if(document.getElementById('ta_' + index)) {
          let ta = document.getElementById('ta_' + index);
          this.editors[index] = CodeMirror.fromTextArea(ta, {
            theme: 'oceanic-next',
            lineNumbers: true,
            mode: 'text/x-java',
            matchBrackets: true,
            height: '30px'
          });
          // let { clientHeight, clientWidth } = this.editors[index].getScrollInfo();
          // console.log(clientHeight);
          
          // this.editors[index].setSize(clientWidth, clientHeight + 100);
          this.editors[index].setValue(this.code_samplesArray[index].code);
        }
      }
    });
  }

  run(id) {
    let code = this.editors[id].getValue();

    code = code.replace(/(public)? *(class ) *\w+/g, 'class Rextester');

    this.slideState = 'in';
    for (let index = 0; index < this.outputSlideState.length; index++) {
      this.outputSlideState[index] = 'out';
    }



    let body = "LanguageChoice=4&Program=" + encodeURIComponent(code) + "&Input=''&CompilerArgs=''";
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    let url = 'http://rextester.com/rundotnet/api';
    this.http.post(url, body, options)
      .pipe(map(response => {
        return response.json();
      })).subscribe(data => {
        this.slideState = 'out';
        this.outputSlideState[id] = 'in';
        // console.log(encodeURI(body));

        if(data.Result) {
          if(data.Result != "") {
            data.Result = data.Result.replace(/\n/g, '<br>');
            this.dataBack = data.Result;
            }
        }
        else {
          data.Errors = data.Errors.replace(/\n/g, '<br>');
          this.dataBack = data.Errors;
        }
      });
  }

  copy (id, code) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = code;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.outputSlideState[id] = 'in';
    this.dataBack = "Copied successfully to the clipboard.<br><br>";
  }

  closeResult(id) {
    this.outputSlideState[id] = 'out';
  }

  save(id, code) {
    this.inputSlideState = 'in';
    this.global_code = this.editors[id].getValue();
  }

  save_after_title_entered() {    
    if(this.global_title != '' && this.user$) {
      this.db.collection('students_code_samples').add({
        code: this.global_code,
        title: this.global_title,
        date: Date.now(),
        uid: this.user$['uid']
      }).then(success => {
        this.global_title = '';
        this.global_code = '';
        this.inputSlideState = 'out';
        alert("Saved Successfully.");
      }).catch(err => {
        console.log(err);
        
      });

    } else {
      alert("You must be logged in & must enter a title.");
    }
  }

  cancel_input() {
    this.inputSlideState = 'out';
  }

}
