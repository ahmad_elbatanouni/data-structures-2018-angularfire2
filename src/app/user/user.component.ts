import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../auth.service';
import { trigger,style,transition,animate,state } from '@angular/animations';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  animations: [
    trigger('slide', [
      state('in', style({opacity: 1, transform: 'translateY(0%)'})),
      state('out', style({opacity: 0, transform: 'translateY(-200%)', height: 0,})),
      transition('in => out',animate('200ms ease-in-out')),
      transition('out => in',animate('200ms ease-in-out'))
    ])
  ]
})
export class UserComponent implements OnInit {

  user$: any;
  messageCollectionRef: AngularFirestoreCollection<any>;
  message$: Observable<any[]>;

  list_of_users = {};
  users_array: any[];
  
  names_array: any[] = [];
  added = {};
  new_messages: number = 0;
  graded_answers: number = 0;

  constructor(public auth: AuthService,
    private titleService: Title,
    private afs: AngularFirestore,) { }

  slideState: string = 'in';
  ngOnInit() {

    setInterval(() => {
      if(this.titleService.getTitle().includes("truct") && this.new_messages > 0)
        this.titleService.setTitle('(' + (this.new_messages + this.graded_answers) + ') new notifications');
      else
        this.titleService.setTitle('Data Structures 2018');
    } , 1250);

    this.auth.user.subscribe(user => {
      this.user$ = user;
      if(this.user$) {
        this.messageCollectionRef = this.afs.collection<any>('messages', ref => {
            return ref.where('users', 'array-contains', this.user$.uid);
          });

        this.message$ = this.messageCollectionRef.snapshotChanges().
        pipe(
          map(actions => {              
            this.list_of_users = {};
            this.added = {};
            this.users_array = [];
            let x = actions.map(action => { 
              let data = action.payload.doc.data();
              let users_array = data['users'];
              
              if(users_array[0] == this.user$.uid) {
                if(this.list_of_users[users_array[1]] == undefined) {
                  if(data['is_read'])
                    this.list_of_users[users_array[1]] = 0;
                  else if (data['to'] == this.user$.uid)
                    this.list_of_users[users_array[1]] = 1;
                }
                else {                  
                  if(!data['is_read'] && data['to'] == this.user$.uid)
                    this.list_of_users[users_array[1]]++;
                }
              } else {
                if(this.list_of_users[users_array[0]] == undefined) {
                  if(data['is_read'])
                    this.list_of_users[users_array[0]] = 0;
                  else if (data['to'] == this.user$.uid)
                    this.list_of_users[users_array[0]] = 1;
                }
                else {                  
                  if(!data['is_read'] && data['to'] == this.user$.uid)
                    this.list_of_users[users_array[0]]++;
                }
              }

              return {id: action.payload.doc.id};
            });
            
            return x;
          })
        );// End of if message$ observable
        this.message$.subscribe(msg => {

          this.new_messages = 0;
          this.users_array = Object.keys(this.list_of_users);
          
          this.users_array.forEach((user_id, index) => {              
            this.afs.collection<any>('users', ref => ref.where('uid', '==', user_id))
              .valueChanges().subscribe(user => {        
                // list_of_users
                //      a key:value object
                //    user_id: $number_of_new_messages   

                // the next line returns the total number of messages from all the users
                // this.new_messages += this.list_of_users[user[0].uid];                
                if(this.list_of_users[user[0].uid] > 0) {
                  if(!this.added[user[0].uid])
                    this.new_messages += 1;
                  this.added[user[0].uid] = true;
                }
              });
          });

          this.afs.collection<any>('students_answers', ref => {
            return ref.where('uid', '==', this.user$.uid);
          }).valueChanges().subscribe(answers => {
            this.graded_answers = 0;
            answers.forEach(answer => {
              if(answer.status != 'uploaded' && answer.notification_read == 0) {
                this.graded_answers++;                
              }
            })
          });

        });
      }
    });
  }

  showDropdown() {
    let dd = document.getElementById("dropdown");
    if(dd.className.indexOf('is-active') > -1)
      dd.className = "dropdown";
    else
      dd.className = "dropdown is-active";
  }

  onMenuItemClicked(e) {
    document.getElementById("dropdown").className = "dropdown";
    e.stopPropagation();
  }

}
