import { LecturesComponent } from './lectures/lectures.component';
import { AssignmentsComponent } from './assignments/assignments.component';
import { CodeSamplesComponent } from './code-samples/code-samples.component';
import { SavedCodeSamplesComponent } from './saved-code-samples/saved-code-samples.component';
import { MessagesComponent } from './messages/messages.component';
import { GradesComponent } from './grades/grades.component';

import { AuthGuard } from './auth.guard';



export const routes = [
    {
        path: 'lectures',
        component: LecturesComponent,
    },
    {
        path: 'code-samples',
        component: CodeSamplesComponent,
    },
    {
        path: 'assignments',
        component: AssignmentsComponent
    },
    {
        path: 'assignments/:lecture_title',
        component: AssignmentsComponent
    },
    {
        path: 'saved-code-samples',
        component: SavedCodeSamplesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'grades',
        component: GradesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'messages',
        component: MessagesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'messages/:id',
        component: MessagesComponent,
        canActivate: [AuthGuard]
    },
];